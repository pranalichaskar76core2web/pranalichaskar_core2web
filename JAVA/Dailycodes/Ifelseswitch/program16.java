class Switchdemo{
	public static void main(String[] args){
		int num = 2;
		System.out.println("Before switch");   // before switch
		switch(num){
			case 1 :
				System.out.println("one"); // false
				break;
			case 2 :
				System.out.println("Two");  //true
				break;
			case 3 :	
				System.out.println("Three");
				break;
			default:
				System.out.println("In default state");
		}
		System.out.println("After switch");  // after switch
	}
}






