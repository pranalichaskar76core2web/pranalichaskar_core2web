class Switch
{
	public static void main(String[] args)
	{
		String friends = "pranali";
		System.out.println("Before switch");   //Before switch
		switch(friends)
		{
			case "priya" :
			       		System.out.println("priya");
				        break;	
			case "smita" :
			  		System.out.println("smita");
					break;
			case "pranali" :
					System.out.println("pranali");   //pranali
				        break;	
                        default : 		
					System.out.println("In default state");
		}
		System.out.println("After switch");  //after switch
	}
}

			

