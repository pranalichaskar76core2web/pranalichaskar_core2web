import "package:flutter/material.dart";

void main() {
  runApp( const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const QuizApp();
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState() => QuizAppState();
}

class QuizAppState extends State<QuizApp> {
  int currentQuestionIndex = 0;
  List<Map> allQuestion = [
    {
      "question": "Who is the founder of Microsoft?",
      "options": ["Steve Jobs", "Bill Gates", "Lary Page", "Elon musk"],
      "correctAnswer": 1,
    },
    {
      "question": "Who is the founder of Apple?",
      "options": ["Steve Jobs", "Bill Gates", "Lary Page", "Elon musk"],
      "correctAnswer": 0
    },
    {
      "question": "Who is the founder of google?",
      "options": ["Steve Jobs", "Bill Gates", "Lary Page", "Elon musk"],
      "correctAnswer": 2
    },
    {
      "question": "Who is the founder of SpaceX?",
      "options": ["Steve Jobs", "Bill Gates", "Lary Page", "Elon musk"],
      "correctAnswer": 3
    },
    {
      "question": "Who is the founder of Boat?",
      "options": ["Steve Jobs", "Aman Gupta", "Lary Page", "Elon musk"],
      "correctAnswer": 1
    },
  ];
  int count = 0;
  int selectedAns = -1;

  Color? getColor(int index) {
    if (selectedAns != -1) {
      if (index == allQuestion[currentQuestionIndex]["correctAnswer"]) {
        if (selectedAns == index) {
          count++;
        }
        return Colors.green;
      } else if (selectedAns == index) {
        return Colors.red;
      }
    } else {
      return null;
    }
    return null;
  }

  bool isQuestionpage = true;

  Scaffold page() {
    if (isQuestionpage) {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Quiz App",
            style: TextStyle(fontSize: 40, fontWeight: FontWeight.w900),
          ),
          backgroundColor: Colors.blue,
          centerTitle: true,
          toolbarHeight: 80,
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 40,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 280,
                ),
                Text(
                    "Question : ${currentQuestionIndex + 1}/${allQuestion.length}",
                    style: const TextStyle(
                        fontSize: 35, fontWeight: FontWeight.w900)),
              ],
            ),
            const SizedBox(
              height: 60,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 200,
                ),
                SizedBox(
                  height: 60,
                  child: Text(allQuestion[currentQuestionIndex]["question"],
                      style: const TextStyle(
                          fontSize: 25, fontWeight: FontWeight.w500)),
                ),
              ],
            ),
            const SizedBox(height: 20),
            Column(
              children: [
                SizedBox(
                  width: 300,
                  height: 50,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: WidgetStatePropertyAll(getColor(0))),
                    onPressed: () {
                      if (selectedAns == -1) {
                        selectedAns = 0;
                        setState(() {});
                      }
                    },
                    child: Text(allQuestion[currentQuestionIndex]["options"][0],
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400)),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: 300,
                  height: 50,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: WidgetStatePropertyAll(getColor(1))),
                    onPressed: () {
                      if (selectedAns == -1) {
                        selectedAns = 1;
                        setState(() {});
                      }
                    },
                    child: Text(allQuestion[currentQuestionIndex]["options"][1],
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400)),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: 300,
                  height: 50,
                  child: ElevatedButton(
                    onPressed: () {
                      if (selectedAns == -1) {
                        selectedAns = 2;
                        setState(() {});
                      }
                    },
                    style: ButtonStyle(
                        backgroundColor: WidgetStatePropertyAll(getColor(2))),
                    child: Text(allQuestion[currentQuestionIndex]["options"][2],
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400)),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: 300,
                  height: 50,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: WidgetStatePropertyAll(getColor(3))),
                    onPressed: () {
                      if (selectedAns == -1) {
                        selectedAns = 3;
                        setState(() {});
                      }
                    },
                    child: Text(allQuestion[currentQuestionIndex]["options"][3],
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400)),
                  ),
                ),
              ],
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (selectedAns != -1) {
              if (currentQuestionIndex < allQuestion.length - 1) {
                currentQuestionIndex++;
                setState(() {});
                selectedAns = -1;
              } else {
                isQuestionpage = false;
                setState(() {});
              }
            }
          },
          child: const Icon(Icons.navigate_next_rounded),
        ),
      );
    } else {
      return Scaffold(
          appBar: AppBar(
            title: const Text(
              "Result",
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.w900),
            ),
            backgroundColor: Colors.blue,
            centerTitle: true,
            toolbarHeight: 80,
          ),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 50,
                  ),
                  SizedBox(
                      height: 400,
                      width: 250,
                      child: Image.network(
                        "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJQAlAMBEQACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAEBQMGBwIBAP/EAEcQAAIBAgQEAwUDBwoEBwAAAAECAwQRAAUSIQYTMUEiUWEHFDJxgZGhsRUjQlJyweEWMzQ2dJKTstHwF1Ni0iRDY2RzgrP/xAAbAQACAwEBAQAAAAAAAAAAAAACAwABBAUGB//EADQRAAIBAgQCBwgCAwEBAAAAAAECAAMRBBIhMUFRBRMiYXGRoRQygbHB0fDxBuEjQlIzYv/aAAwDAQACEQMRAD8AzvjNcop63NIYxJPmjZhLI0yllRFLG6FWG59RhFPr3qXOi+vjDbIF03jnLMlmly9VqpGoOHYFSSSoAsaxnG5Dd/IKPT6c56wLXUZqhNrf8/m+s1iwFr2Uesos8ccLyOjLZJtKwvcuV3Nzbb0O/XHYBNrHz/PSYe+EV9R+WczRqakWGWYqmlXZtbk2vv06jYbDCkXqafaNwIRu7eMfcT12VUuVwZXldNElTGDHWkrr8YturlQb9b4y4WlVqN1tQm247/hrwmitUVRkW3l9Z17N6uoy7iWCN2eBJG3LJuGA26j1+/GyoMylk962n4dJmvawbaWn2yZVUU2b02b0jgSHxcxLLa/TFot0CVDc8ZV9cyiwmZQmGlp2kPNXMYqhGjFvAFAJJPrfT9+LZWY5T7pBv+ecga2vGT56vKzCrgMCo5m5hNiGTULlPIAX6W7YDDglFJ8PpeMqkZjad0eUyVma0FPlxe84VlmqkCJcC7tvcFFsd+9vpiGoAjF+HAeg8T8N4AGukm4xzQ5pmYKVstVBGiqHbUqM9gGZEJ8IJ7YVg6Bp07stmPn3XPEw61QM2hh1DV02acOTUNXQyRvSQfmquGoYKz38CtGfCSTtcWOFvTalVBpm5Y7Hlx17t/SEGzoc2wE0/KhS5B7PRRvURwTTrqlLKSVW172Aw+riArBF1a+0WlFnBc7TFaUxRpLXTwrOA+hI2YgFjc3NtyBbpte+GMbtlHj+ecg2zSzrmFZk/BRlp1SoizaRxNUXS9PN+qLDUrWAPW1jjB1K1MSQ2mUWAtuPla/deaDUy0tNz6GU+lqDT10VW680xyiUhmPiIN9zjoOl1KjSZgSDeEZhmldmubNmNRIXq3k1ggDY3uAB5DA06SImRdpZck5jO3y+Z2rat56aVafTK5D7S6mAsuwvu3p0OKzABVsReTiSZDm0MNPXyLTEmBgJIr/qsAR9l7fTEpMWQX34y6gytpLJ7Rcwmrc6ehqJzVTUdTODKxuVDPcR37hQPvIwnD0xneoBbN9L6/H6Q6lgqrxH1knDGbU2VLyHMTFVaallm1OIp9JGqyg+G+/Tbb1xWKoFwSo30O2o9NvGVSqhdDw1ErldTTy5myzyCWaeW7SR+PWWPUW69emNaWCDKLRRN21M7igp5KuOmpy6LBJK0lWAdTIu+rT2sq9PM4WxYKWbjbT6fGMUKSB6wjOM4bOMzlzGZ5FqFSNYWAF20WAZz+tYXuO+JQoGlTFPca3+w7uEF3DEmE0+Yw++ZVJBJO8ykvUNK7E807N1JG9gb7dbHpgqVNldi23Dw/NIFRgVAm1cY0MOc8LAPEskqJZN7/LfDhZhmESNGtMW5kcvEdCwaCSZJRcJTrDErg+BQBsRcDf1OM9RCtFx/ZtxmmnZnEDidhOk0nLqcwqnEgd2IaCXmHduxJO5B2scMZcwI2UeosNoIJB03hUmdSy1LCuVYxT0VRTRRwr4Qz67/LxOfswo4XsDIb3ZTryFj8hD67tG+mhETVKQ6k93MhGga+Za+vva3byxqUN/tEsV4S1cKUArosvgUqyrNLM8Q7vsFv8AQffjMwZXqPbWwt8466FEUnS+s0f2hCKk4blg0yN+YOnli7TSW6/sr1Py+uMVGreoEpar/sx491zx4WjHF1JffgvKYOSQLA7XuBfbHWy6zNc7Q+qqPeIVo6YpFSQLrsW081gN3N+rHsMZ0p5TnbVj6DlGO9+yNhCMxpYvyJBXLT08HvDhY0jDEjQCGuS3e4bpv2OxGBRm6wproPt+t5ZAy3keTraknVghjkdOdt4tA3sD29fPDiuoMC+lobmVDQ66afLVkiSotCI5WU8yTa5AG6ruOuFlyoYtw5cpYF7WizPpaabMClDq90p0WGFn+J1XbUfmbn0vbFUQVW7DU6mWxufCF5/V8riTO0WGNmevmOtr3A1sLfI3+7DAO+UTA6pp1MFVLPFKWTSQmzR6dgrCwsdsUlrkAfndIZeK3gqGl4Vos8fM/e6uaePVBR6XjgBXUQ7jcNb78DWqFEJmzo3DjEYpKZ2vc+A1iiRKeeZhLDHq6fDbtjlio6gEGe49kwtVijoDbukLZRRt0jK/sscMGNqjjE1P49gX2W3gTPjlCGVJGnk1ppC3A6CwA2t2wxMeRoVmCr/FqZ9xz8bf1NmyOI1GQJA0gI5ek3GN6YkML2nnMR0Q9KplzekyriHI2hqxTFaeHRI7ipRDrNwLK1utiNj2ue2ENi0ViddeHCbKPQGJqIGRh6/aJ1y6aWv59cdSvJqlMJ0t81uLX74oYyilPKg22v8AuMb+OY5mzEr5n7SOXLqqRWDtE7NIZDI3xsT1uep87eZODGNoDa/hwgH+OY4/8+Z+05jyVykhkkUMF8AAJub9/La+KPSFK4sDCX+MYziV8z9ppPs5yVl0VMoVXRSCw/SNzbt2BwyniVbWZMV0NVokAsLw7jUhIqumhqVp5qvSDJoPw/pC46X8Pbtjm4sgVg9ri97d+gB+86GA6JqVUAvsLaDW3xMzaDJ6anlilBaQoQQrgFbg+XcYJ8dUYWnWpfxrDLqxJ/PCS/kmliVX5EZDXtc3O3p2wo4qq2mabKXRGAQkCmLjnr89JxU0C1cUcaowCSA2jUagvcKPO3Qdzh2GqsKgud5i6ZwVNsGzIoBXXQcBvG/tF4fi4UFIKbNxXXAiWN9CywIFvokQb9xYn1HljoKtm02PzniSRbvlSzGkfLTIK55Yc0jMUkAWzRPEy6gQR5eG3br3wAfrSCo7Jv4374VivjAczkhnrpZqUaY5Tr0kW0ki5A9Ab4umGVQrby6hUtdYZxGZYeKM1qIgRozGXS+m4DCQkenbB6HSB3yGNqiekrqqcSTRu4MjLIBaUklWYdx8X29cUQuZVG/0lgmxmi+yd4qerzHIq8ucvzBAE5sZS7EbMAcJqVA1kPGdXA4WqKb4qn/oR/cF4k4fmyvOJ6WQHUCSrAfEt9jjmvemcpnssO1PFoKymxM9oMtjp6enqM11iKqk0Q6ZAlgD4nJI6Dt54UxDEhYD4isrNTTUrvpeL57JM4j1tGSeWzLYsvY4lpvp18w13mh8GVwkoNF72xvoHszg9IKOtvFvG9OGPPCi+FYhOM2dFVgvYMq1FTyV1VFSwKDLKwVQf34xnQXM7FWslJC7bCcSRGOZojZmVyt13BN7bYkIVFKhhCK2hkoswNFMYzKpF9DXAuL2xF7WoiaWKSpS6wbTTsmoTQZbFrdELRCUq230HmcdKlos8licSK1U2HG0o3Fzz1WYMscUj6LatKE2vsL/ADOMtc9qd3o9lp0tTvEdTS1FLGZKmPlrrKaXIDXHXbrthIsdpr9rpubAyeoqadeHKeOOLVM1S7u2nci1gL4tUOfWYjVbr2I1FrSz+z7LIoaObibMYwkFOGMEbbgt2642UkC3c8JzsbiGrImEp+8+/hMnzqvrJM8lzaWRGqJp3lBJDEEMRup6dOhx0OxVUjgfrPHvTahUKncEjykdRU1Pv9JU1DU1W+hDHEhVkC9k0r8PywCogRlXTv8ArKLEtcwGqieKpljmh5Lq5DREHwHy38sMUggG95RFjrGvGUsTcT5xFRmZaf3120SN+mCQx+0tb0OKpg27W8o24bSPL8n95yesrpZGi5Ka4SVuktmAddXZrEEDvbC3rhaioNb78+74b3+EIUyVzR7k0skFBBV86RqkyCTU177WtufQY5+Ib/PoNBPc9D0CvR9mHva+f9WmqZ9T/wAq+F4cyojauhjCyAHdh5HGgqtanmtOUpfAY00xtKSc5pKgZdBmlPUA0QEbqgBVxfruf3b45xokE5TO0jVUzlf9tb8o7XOsnq5KsmZC/wAMTMhXwuwBAHYBVW/TvbCurcWmcUHAW3x15fc39Lyz8PPQTVtU0LxSIJAuyeERIvbaxuep+nbGnDlhoZzcWtRaa3uDbnxJhWZU+XVcUNNNFA3PTd1A8JJBO/ba4HljS4YqTE0mrIS6naIUpqSiZ6mkWiimWCplBRgNwdKr8h1PrjmkMTY906JapU7DEkXUcfE+cj/JmWRzJqNGZFeljMjMtzvqZ7/rN0v16YolvnC66swO9u0f6+EnoYKGeeSoleikqJDUyEvpZg2rSov1AC4bRVs0CsaqqFANhl+Vz6yyT1NFEknMqFZLxC3U6FG/346GoE5i0arWAXnKTWZpl4nrDK8k0iOjcyPVpkAdza58gw+o2xz3Vi1xO7TwNUqvAG+/DQD1sYizfM8pqKOaGnim1tUc1WksNIN9tifTbFIrgzVRw/VOGqEEW4c/KRZBTzZ9NS5XT00axJIWabTZiDbYnGmlSzPrFYzEjDhqqcRaWv2h5hFQUVDkFAwEUdg+nvscPxLALkWY+hKLM/tVTczLnGXZbnNe2cUnvCmmYRQqSCZTsCGGwtub/Zg6Zq1KKCmbWO/d9fD1nF6aSnTxztuGAI+MrbRNyzULpWPmaVGre/Xbvttv6jG6+tpxu+RsxZizsWYm5Lbk4m0l42z4n+VGdIkCyvJVzqAVJK/nCbrbvtgT3mWItjVpGSJLlmYBVHcnDLgamCASbCXarcInjijgKWBRF0qltjt9McFe0+ms+nBVoYRVv2VG8f8As94tjp8zkoNngmGzEnr3GOphsM6r2tO6eN6Z6UpVsQGpai2+3Oe8dUVPS1JrYkKxSG5st7YXVwbk3SbMB/IMOqZK9x8LysCWmLC0seqwIsw74zdRWGuUzsL0j0fU2qD4m0tfD1c6IsSRFda8szBj41uTYdvn54lNGDaxOIp0nGZXv8RLVo1qFPQi2+NZOk5wJGsqWa0eirawBF+2MjjW87FCrdNYFLSE/o7jbcYDLNC1u+POH6QxprPxd8aKK2vpOdi6xY2jPMXKUr7kbWw1jpM1MEtKFXHlu5mdowd/EdP44xhHY6CdY1aFMAu4HxEHmSGJo43dDJKoaNImWS9/1tJ8O3n6YJMPWqe6JlqdM9H0tc9/DWaPkTU3D2RPO8YEpS5ZvDf643UsHlU5j5TzOP6YXEVOwDbvmd5hmy5lmcrPOksokNyjalNu6nuMYq9NwS5GhnpuicdQq0qdFT2huPzhFWb0stRV0Zp6czyM+kRAX197fccHgqgVWDGwmP8AlFG7U6oHd9fvK65MsskioEUkvZeijyx0rEC08je5vC4q2qy0NTPSUyupuy1NIjOCfPUL4SUWp2r+RhhyotD84kqaLjPNWpqqSGX3uf8AOU01jYsTbUp8uv34MqrrZhcd8E6SbLadp6eI0gM9TIysIVOyBd2277DcnEq3y72jMOyrVUsL2IPkbxgsUmZsaKkCGpl8KKXGkn5ntjlXWg2d/dG89xiMT1+CqIfesf3IK+mrsszlZ83lhDx6L+48sg9bKNNlvYG/lcdbjHVoVUK5aI878La87cp4GpTO7HTul/pqqDPcqsNTBxax3PT0xsVQt2aYncmwEp2eaoYEyx8vpYFil5iyQxlWfa3ive/0t8sLo4YLU60OTcf3pGPXzJkIl29n1LDX8PVVBP4eV+cVm333N8XVPVXdzpKp9qyqNYdCpa8ZSUzfChElhbztbf7cZXqioi4hHAS+umvhx8rDxj1z03NEg5+Gv6/OEmh4TqM3qquE1XKlptmGsnxYr2zC1GKKL27o5fbqagmoRfvMB/kNnZl0Cqga56mVrk/Zha47Bk5ch8oZfpAD/wBD5mFT5FW5TSA1TEkvpBSW4PntjTSxOGaoqLbXTaZKhxmQs7nT/wCoZk3DzV3IqpZCIAxL6r9vnhj16XWGkPeikWoy52Okzzj1DU51PKwTR8MJBvcDrth1PUW5QH3uZzwdkqGT3yoXofCLYMiDeF8aZzEiw0Tq8sT35saNZtPz/wB9MZ3zMCKZsRx/r5+M0UgLguL3lXjRKrNqR4KYUNOaNUjeYCIS6FAaQ9vEwPe3rjnVwy0CpNzm4a2vew8v1PQdCuqYzPbQKZFUzVE0iLSSCORDzI5A+nSANzf5H63wrDUwpOadLp3GCrSCjncfD9xa8tLTUksGmTmOhAIt1v39LX/316LZri08qLcYtkmeZzJK7u56s7XJ+uBsBoJI74hq5E4qrlqF50NPXz2iYlQQZGJF133xQQaldCZZY6AwSaVoEPu6iFanxaVkuQtyAPMfXDFN7jlBNxOKY84OZqgwrHCQllLayOi7efniwmX3Rufwwmr1Htdthb4co34fWmhqaeapopa6lGoGHSdAkHmB8QAsSMLxXWZCEYKdNb8PpylUcuZc1yD85e8ornmiD09NQU8LSEBqekanKkC5ALEjYW6HvjNh1y1yKjMTa9iwZd7DYfPxkxCjJ2LctrGeZxQir8Vg7g6gfPHZzBUuRb82nM3a17xlwNmKQiry0xBFdeYu3iYKbsnnut/sxz+kqYCrUfVQRfwmvBVLvlG52loz2iRX98pR4ANSqpsOnljNjcFRFB6irZgL6fbaOwuKq9ciE6HT8O8J4YqdXGOcRfozRRTr8ioBxycO3+UHmJ064/wjujWmqFNcy7eE/hjOjjrJbL2YlzmX3+lpI9/zk0j2v2BsMaMFTXEV0VttTFYioaFJmXfQSSd0yulhlkZi8UbuxLE/mwPh+RJX78dbFYekuSlSFiTp3czec+jWcq9Sobi2vjM6hyvL8xIqMwq6l6mRtTRU8alYxfoSe/yGHYjHVqdRqaKBbYsSL+FhaLo4XrKYcknwtcfnhDJpocuhc0xYJH8Ja1/sxtRalRR1oHw2/Uz5kUnJf48pm2aV09RXy1PMfU+oau5B2P3YMotsttIalhrA5EhYjlzuoWn31qfE/XQLdt/lhBDcRx9Oc0hhwbh+CdQjmxU0cD8ysdtCoPDpHbc7G++FmyZidBzjGqNVCgm9hbwkeeZRWZRUxw5ioWaWJZrB9RAbsT5+mApVkrAledpGQqbGWrJuE8mrcppKmRqiSSWMM/IdmCt5HwbH03xy6+LrJUKi3x/c1pRQqDYxLxY1FS8SV7UqtUMamo5pmSyFixHhHpvv546CCo9w+mult4h8i2y6xRWUho5+S0sMp0gloX1DftfzxppPnXNa3jEVFym0nqZhPIs2tnkdQZCVA8XTa3a1sHSXKLDbhAdgTeMMo9/oq6nnoZ2pJShdJdQsiEEEnrtYHbr92KrGk1Ng4uNrfn53y6YcMMptxjafOKnMswppaioeRYlEYkkNr73LW7X/AHDA4XCphKTBV77D5d8GvV65xr3XPzl2y6ograVDcIAL6ipJP3/uw1+uXtWzW79vhb6mZwUOma1+7hIvdpKGphroGW4e4AYX28x5Ye4SsGpMDtbY285mVmQiosvOV1EdTSNTMOi3QH9T+HT7McvCsWVsLV95dD3jgZ0a9gVxCbHXwMW5Y5oeMsukYkCWFqVyfNTdftGPPU70auRt1M7bEVaBYcdZwcxeLNs5AP8AMawPnewxncZXJ8Y4LdFjLLoAGiMp8FLCEJP6x3b92O10NTtmqnYafUzkdJ1LkUxuTf6CJ+KatpqZ1G3PIAH/AEjp+8/XHTwINeq2JO2y+HE/GYcWwpIKA33P2iqgWnpIYb0KPKpNmLEhze41D6264LF0qxq/46pAO62G3HXh99oOGamafaQaceN+A7/tKxxVVOpen2V7kMB0v3GN9MKEATa2nhEEszEtvK6K8DLXpGoI55FVlSZ7nkqSCxCjvfbUel8ZGoDrusD2Btpzttr9LTYtW9PJba+sVPDrljhpiZpJLAALY6j+j679++GFrAltLfl5QGthIq+iqKGVYqtFR2QPpEivsel9JNj6HfC1qK4usMgqdZBGr1M8MGtvEwjW++kE9h9emFuQilvjGLdiBLFnHFFdQZnU0ORv+TsvpZGhhgiAOykjUxIuWPUnHNp4KjWUVaozM2uvfw8BHmq6nKptaNPao8jVEHNgSlAqag08Gu78stu7DT+kRcb7C23fF4FctWotyeZt6Dwl1iWRWMoqdsdZZjMZwAvl4hEMBaSqULMzgOCFI0+inUDc7XGJb/Je52+vzkPuySmjWMEzxyAOoMZ6A77n1GxG2G3JIykd/lt4xZAAOb4RsspcLUtIpZCgRDubAbeltgMWtMAdWBob/njrBNQ3Dk6/aWmiqCUp5xQCngm1WfmajM2o6iT6dALC1sBhhkvTz3YDloNNPpfXWKrtnPWZbA+seIgkTTvvjQRkJqNy1sNZmvnAQc9NdI7ymLXCE1MsiHwOO2MuMworEOpyuNiPl3iNwuJNIFSLqdx+bSPOIK5JFrZY4isDpIrxk3JU9wfTbHnekcLWB65rG1gbehtwnewGIokdSl9b2B+V4qgZqviXNhBZubUhhvswsG/Ej7MYSjV2RU3M6LOtKlmfYSz09LUrAErWjRBuY4iTqPmTj0FLAVSi06hAQcBx8TPPVcbTDmpTHaPE8PARLmqias1AeFRYDHbRQq2E5TMWa5iuodY0dibGxI+eJkCkkDU7/KMDEgKToP3KPnBaeUsNzvi942LK2qVqaKCOnigkQFZJYi15gbbNcny6Cw9MZ+qIYsxJvwPDw/L95js/ZAAtFXKklcRxIzu17KoJJ+mIxC6nSEtydIK1u33YFoc6jcU4jqYZStTHKGVdFwLbhr/PtbGeoua6kaERq6a8YM7tI7O5LMxJYnuTgbAaCFe+ssPGmYLW8QZkstMoqI6t4+ertd1QlRcE26AdLdMLoUTSPZOmptpxN9994TvmGu8VVVS1XMJHjijIUKFijCCw+XU+uNFJAgsPWKqNmN4RTw0ppppJp3SdSOXEIrhx3u19vsOGXbMABpx1i9LHXWF0s8QilE0Rkmsgp2LXWOzXNx0II2t64M0zcWNhrfygBgOGvCMAlRLWSGpvztVpLjp2/DDKQRaYFPbhE1CWclt47y6nWGQslmubBrWvhouwF4ogC/GW6kAsNh9DgaYSxdRvr9IFXNcIeH7jzLRaQdb323tgK3WWHVkD8/OcqlkBOcR9y1miMbfCwIIwmpQR1ZT/ALaGNSqyMGHDaLssooRUPVLFCkjAAlIwt9rb/QY4nRVINWZv+dB8Z2Ok6rCkicDDKtvAflj0QnBaVyojeSYRxqWdz4QOpxVSulIjNsb+kKnRaoCV4W9ZW851WaKw1X39MNuDZgfw2lLppaVyog1SLGCq6ja5OwwLtlUtNCLc2iKrgj98MU0ywKGIaQguFI/ZuT9MAzHJdRf0+cML2rGL+ZJBIJYZGR1uFkS49PwwLAMLGGpI1EBcWFuwwDQxD8m4bzfPuacqo2nWEXkfUFVPmWIGMdfE0qP/AKNaPRGbaSVXCWcUcnLqoYInI1BWqY72/vYyJ0hh6guhJ+B+0c1B1NjB+Jv6z5v/AG6f/Ocb1iTGdYcrqEozSUiwJT06iWXdWqH7tpvsO3mcBh0qqSXN7nxtJUZSBYRdLGHU1EJLIWOsBT+bufDc9N7Y1q/ayn4d/wColl7NxO4Rcjph4mcx/RxCCVonZGKHqjXU/I4it1iZhpeUew1jwj2nA03Sw36A4tDYhG103/OMXUFwWG0s2WQMQ+mzBTuTiiWKi41O/wBYFhm0P5wjqmjB0MCw3BAvbFMikAcoIcrdhxn0vGGU01dPQPMVngQlnawj1fqaifi3xixmJalTPVi7TbhcL1jDObCc1PEeV5WhlbMaRokUl0WojZjYX2sdzsbY4WBq1qFY9k2O+k62Lo069IWOo2k2WZ7Q8Q5aK7LXYxFirK62ZG8iPrj1KMDrPPVaZQ5TBptSKbmx7lT0xFQsv+WxN/15SZgG/wAdwJXM40dFuWXv54IZs/MH0/cI5cum49f1KtmcckUmmUEGwNgb7Hf8MEGDC6mTUGxiLMOXzpOSGWO50hjcgYFc2UZt+6NNrnLtF0ssgi5Qc8vVr0X2va1/swBAvm4xgJtaRQ07Sm+kstwLDuT0HoPXCmNo1RLHklHqhnoJ8yoaNzIrx0lTG0kcxItqHXfa3njl44ZSHCEnmDYgfabcO2hXN6XiHilKOPOp4suA5cVkdlj0K0g2cqtzYXvYemH4YsaYLnfbw4Xg1bZrASTPUR+K83DsFArZ9z0+NsOEUZCMykEFRAqx8udFRiV3sHD3Hkbj7zg8gJB5fa31gFrAiRRuwUrqbSeov1w8AXvFkm1oaKgzMrMkSFVC/m4wgNu5A2v64NFC7RTm8aZeSzBFF27KNycOGgvEGXDKsozKexjy6qII6mFgPvtgGrUl3YQLXluy7JsyQ6mpmUH4lLAXxnOLo85VrR1T5bVhtUqKPrgDjKO2sEgyucC5BlNfDmUmY0MU7tWyqzP6H0+eOVWxDrXKg6WvPQUqYNIHjPs+4U4biaQR5TAtx4WBa4+/HNr46qrWDTXTw6MNRFvswogJuIKelVUjjqUAS/w+HHdwmJAoqz7mcXpJAtWwlsqsqrGU8uINf1GNgxdLifSYLWlazLI80AYiimb9kavww1cVRP8AtJYyo5tTT0qlaillhN7kyRsv44YGDm6tcd0MEWtaVaqYEm2LMYsGgLo7zxVEUTwrqGs2LdrLtYnfGeqAeyRcGaaVxqDI0qhAjwwTSokyKJGsAdQN7dd1uB67YWVvqw2hggbSZs55FK6xAvVNYGoY9LdSPwGEVaecgH3eXOORgu28R3ub4s2lRnxP/WfN/wC3T/5zi1lGAphoizJ0PnuPLDBFyYEgbHr0wwGARNr4K4x4UjoYooaEZXLYa7xhgxt11jc//bfHKq06rMSTeLqU2EvFNnmU1VhBmVI5/V5y3+y98J6thup8okqYcksTfC6N8mBwJMqC5yak5bOKHSKgxsIyega2334oHWMQrcXmS0+Q8bZZAYcvrWiVjdhFIwBPn0w4vSY3IvOgMQo4wOoyL2gVBIlrpGv51D/9uAKYU6lPSGMWRsxjPgjIOKMhzWpqZxGyVMWmQ62ZmINwdwPX7cXUdCoCxFWoj6tNPoJqo/0sKo+eFCY3y8ITNXUcC3mqoIx5vIq4MAnYXi7GJ8x4s4fpo251fDKAPgiHMv8AZcYPqah2H0hqpmI8eZzlGa5qHyPLPco1B5rEBecxPxaV2H4m+Ojhg4BDG8fksNZV3Onr1w42hiQMThZhiQPhZjFnAwsw4z4o/rNm/wDbp/8AOcWJRg1FTz1lVFTUsTyzysEjjQXLE9sHmA1MAzSsl9j2b1KrJm1bT0amx5UYMr/I9APtOEHGKPdF4hqqjaMKvgPhnJQfe5szrnUbqZFjS/0W/wB+LWvWfawgdYSb2lDhWWilaNkKoD4SNwRhygxzkHaOaSrFrFvtw4TMwjOCtAtpa3ywUWRD48ykHSeQfJyMTKvKBCEzao7Vc/8AinE6tP8AkeQknbZrUW/pc/r+db/XE6tP+R5CVrIXzOUjeplPzkJxMi8hL1gk1eT1lY/NjiZQNhCF4uqKoWJuuKMMCJq2qJ+G5PpgGjkEP4UyfK641P5epagvIy8l4ZtDIO9xuPtGEMai6qYdR9NJaJfZDQVtPzcpziriv0WqjV/8unCfbHBswixWHESjcV8BZ7w1Eairijnow2k1FOxZV8tQIBH4euGpiEqaDeORlbaVB8GY0TkYXDjPij+s2b/26b/OcEJUGy2tqMvroKykmaGeFw8bodwcRgCLGVa+82XIPalVNEi5pRRTW6yQNoP903H2EYQcKD7pmN6QljHF/DmaIFrIZEv15sV/vW+B9nrLtFZCINNlPBeYXZKxIi3/AKmn7mGDDYhd1kzMIDJ7Psln3o86A8vEp/fgvaXG6ws5kJ9m0t//AA+cwt5XU/uxfto4iVmHKcN7OM5H83mdKfmD/pi/bkkuvKef8Pc/X4aykPrc/wCmC9vSTscp4PZ/xFf+mUf2n/TE9upyuxynv/DrPW+KvpFH1P7sV7cku68p2PZtmJ/ns1ph8gcV7csl15SRPZtTLvVZ0vrpUD8TgfbCdll5+QhUHBvCtGb1OahyPOVBijWrNsplF2h8FTwdlW8TJIy9wrOfwtgCuIbcStTIq72j5bTIVo6OeUgbA6UX95+7FeyufeMsU5mfHnHmbZ7StRMY6WiZgWiivd7b2Zu/0thq0EQ34zVSpgaygG57YaY608XAQoz4mUtxRm6qCSa6awH7ZxckF93VApkkYXNtXL8F/K98S8kb0M2g6b7jqMMWJdTHdNUW77YYJnYRjDU7dcHEmFx1A9MWIJhcExcgKf4YsyrSd5xE1kkYHvvibyGeiuk/5zj5OcTKOUGe+/Sf86T++cTKvKSctWMesjf3sSw5STjnF+rNv0tuTiQgIPLKOqm+9j5jEvLtBJakeYxUuAz1Isd8CYwCKauqABJOAMaolbrphO+lTqN+gwkmalFhO5qZaekLSsOa4BUeW+KhQFcVJG+fuI+Lc2Zvh99nB+rtiST1KlVoBFWKrRxkcsL/AOb5b9hiSRTLIZH1nwnsB0Axckkiq6mK2iZh8zfBAmCVU7iFx51XJ0kVvmowQdos0UMLg4irCwUxxsT8xi+tMH2dYxPFU1GNLUw5yns+L608pXsw4GQji+TvRm//AMv8MF13dA9lHOdjjA96Rv8AE/hidd3SvZO+e/yx/wDaP/f/AIYvrhyk9k75y3GDdqM/4v8ADFdd3S/ZO+T03GFQGR4KRCwGllaToL9emKNbuhDDAcZBmXFkjyOtNDGQT4pLnxH09MV1p5QvZ1iyTiCtckDlqP2b4rrGligkEkzOsk+Kcj5ADAlzzjBTUcIM8jyNeR2Y/wDUb4C8IC0LpQtNGKh7Nc2AB3H+/wDfrJcEmlaVrnoNlF+gxUk5XpiSRvxLEDxJnJJO1bP/APocSSLuWvriST7kr5nFyTzkr5nFST4RL5nElT3lhdwTfElz7lht2LEnqTiST7kr5nFyT7kr5nEkn3JXzOJJPuSvmcVJPuUPM9MSSfCFT3PXEknnKW/U4kqfclfM4kue8lfM4kk5CA23OJJOuSvmcSSeiFfM4kk//9k=",
                        height: 250,
                        width: 250,
                        fit: BoxFit.cover,
                      )),
                  const SizedBox(height: 20),
                  const Text("Congratulation",
                      style:
                          TextStyle(fontSize: 29, fontWeight: FontWeight.w600)),
                  const SizedBox(
                    height: 20,
                  ),
                  Text("score ${count}/${allQuestion.length}",
                      style: const TextStyle(
                          fontSize: 26, fontWeight: FontWeight.w500)),
                  const SizedBox(height: 40),
                  ElevatedButton(
                      onPressed: () {
                        count = 0;
                        selectedAns = -1;
                        currentQuestionIndex = 0;
                        isQuestionpage = true;
                        setState(() {});
                      
                      },
                      child: const Text("Restart the Quiz",
                          style: TextStyle(
                            backgroundColor: Colors.blue,
                              fontSize: 25,
                              fontWeight: FontWeight.w500,
                              color: Colors.black)
                              )),
                ],
              ),
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: page(),
    );
  }
}