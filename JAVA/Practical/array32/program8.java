import java.io.*;
class array8 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter rows :");
        int row = Integer.parseInt(br.readLine());

        System.out.println("Enter colums :");
        int col = Integer.parseInt(br.readLine());

        int arr[][] = new int[row][col];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print("Enter Element in row " + (i + 1) + " and column " + (j + 1) + " : ");
                arr[i][j] = Integer.parseInt(br.readLine());
            }
        }

        int sum = 0;
        int n = arr.length;
        for (int i = 0; i < arr.length; i++) {
            sum+=arr[i][n-i-1];              
        }
        System.out.println("Sum of Secondary Diagonal : "+ sum);
    }
}


