class Switchdemo{
	public static void main(String[] args){
		int num = 5;
		System.out.println("Before switch");   // before switch
		switch(num){
			case 1 : 
				System.out.println("one"); //false
			case 2 :
				System.out.println("two");  //false
			case 3 :
				System.out.println("Three");  //false
			default:
				System.out.println("in default state");  // by default it is print
		}
		System.out.println("After switch");  // after switch
	}
}

