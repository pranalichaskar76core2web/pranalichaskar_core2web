import 'package:flutter/material.dart';

void main() {
  print("Incubator");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "core2web",
      home: TestApp(),
        );
  }
}
class TestApp extends StatelessWidget{
  const TestApp({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("My first App",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w900,
          color: Colors.yellow,
        ),
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Container(
        width: 100,
        height: 100,
        decoration: const BoxDecoration(
          color:Colors.pink,
          shape: BoxShape.circle,
        ),
      ),
    );
  }
}
