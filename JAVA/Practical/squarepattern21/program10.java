import java.io.*;
class SquareDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row");
		int row=Integer.parseInt(br.readLine());
		int num =4;
		char ch='C';
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				if(num%2==0){
					System.out.print(num-- + " ");
				}
				else{
					System.out.print(ch-- + " ");
				}
			}
			System.out.println();
		}
	}
}

