import java.io.*;
class array9{
        public static void main(String[] args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size of array");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];
                for(int i=0; i<arr.length; i++){
                        System.out.print("Enter the element of array " +i+ ":");
                        int element=Integer.parseInt(br.readLine());
                        arr[i]=element;
                }
                for(int i=0; i<arr.length; i++){
                        if(i%2==1){
                        System.out.println(arr[i] + " is an odd indexed element "+ " ");
			}
		}
	}
}

