import java.io.*;
class array4{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the row :");
                int row =Integer.parseInt(br.readLine());
                System.out.println("Enter the column : ");
                int column =Integer.parseInt(br.readLine());
                 int arr[][]=new int[row][column];

                 for(int i=0; i<arr.length; i++){
                         for(int j=0; j<arr.length; j++){
                                 System.out.print("Enter the element in row " + (i+1) + " and column"+ (j+1)+" : ");
                                 arr[i][j]= Integer.parseInt(br.readLine());
                         }
                 }
                 for(int i=0; i<arr.length; i++){
			 if(i%2==0){
				 int sum=0;
                         for(int j=0; j<arr.length; j++){
                                 sum+=arr[i][j];
                         }
                         System.out.println("sum of row " +(i+1)+ " is :"+ sum);
			 }
		 }
	}
}
             
