class bitwiseoperator
{
	public static void main(String[] args)
	{
		int x = 5;   //0000 0101
		int y = 3;   //0000 0011
		System.out.println(x&y);   //0000 0001  //1
		System.out.println(x|y);   //0000 0111  //7
		System.out.println(x^y);   //0000 0110  //6
		System.out.println(x<<1);  //0000 1010  //10
		System.out.println(x>>1);  //0000 0010  //2
	}
}


