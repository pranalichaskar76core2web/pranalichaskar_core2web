class Switchdemo{
	public static void main(String[] args){
		int num = 2;
		System.out.println("Before switch");  // before switch
		switch(num){
			case 1 :
				System.out.println("one");  //false
			case 2 :
				System.out.println("two");  //true
			case 3 :
				System.out.println("three");  //true
			default : 
				System.out.println("In defult state"); // it is bydefult print
		}
		System.out.println("After switch");  // after switch
	}
}


