import java.util.*;
class Squarepattern{
	public static void main(String[] args){
		System.out.println("Enter the row");
		Scanner sc= new Scanner(System.in);
		int row= sc.nextInt();
		for(int i=1; i<=row; i++){
			for(int space=1; space<i; space++){
				System.out.print("\t");
			}
			char ch='D';
			for(int j=row; j>=i; j--){
				System.out.print(ch-- + "\t");
			}
		System.out.println();
		}
	}
}

