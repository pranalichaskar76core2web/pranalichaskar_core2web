import java.util.Scanner;

class ExceptionHandlingExample {

        static double getDiv() {
                Scanner scanner = new Scanner(System.in);

                // ArithmeticException Handling: Division by zero
                System.out.print("Enter the numerator: ");
                int numerator = scanner.nextInt();

                System.out.print("Enter the denominator: ");
                int denominator = scanner.nextInt();

                // This will throw ArithmeticException if denominator is 0
                double result = numerator / denominator;

                return result;
        }

        public static void main(String[] args) {
                // Create a Scanner object to read user input
                Scanner scanner = new Scanner(System.in);

                // Try block to handle multiple exceptions
                try {

                    // This will throw ArithmeticException if denominator is 0
                    double result = ExceptionHandlingExample.getDiv();
                    System.out.println("The result of division is: " + result);

                    // NumberFormatException Handling: Parsing non-integer input
                    System.out.print("Enter a number to convert to integer: ");
                    String input = scanner.next();  // This input will be parsed to an integer

                    // This may throw NumberFormatException if input is not a valid number
                    int convertedNumber = Integer.parseInt(input);
                    System.out.println("Converted number: " + convertedNumber);

                    // ArrayIndexOutOfBoundsException Handling: Accessing invalid array index
                    int[] numbers = {10, 20, 30};  // A simple array of 3 numbers
                    System.out.print("Enter an index to access in the array (0-2): ");
                    int index = scanner.nextInt();
		    // This may throw ArrayIndexOutOfBoundsException if the index is invalid
                    System.out.println("Array value at index " + index + ": " + numbers[index]);

                }
                // Catch block for ArithmeticException
                catch (ArithmeticException e) {
                        System.out.println("Error: Division by zero is not allowed.");
                }
                // Catch block for NumberFormatException
                catch (NumberFormatException e) {
                    System.out.println("Error: Invalid input, unable to convert to a number.");
                }
                // Catch block for ArrayIndexOutOfBoundsException
                catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Error: Invalid index for array access.");
                }
                // Catch any other unexpected exceptions
                catch (Exception e) {
                    System.out.println("Error: An unexpected error occurred.");
                }
                // Finally block for cleanup
                finally {
                    System.out.println("Execution completed.");
                    scanner.close();  // Always close the scanner resource
                }
        }
}
