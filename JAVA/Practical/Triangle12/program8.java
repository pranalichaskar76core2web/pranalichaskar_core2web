import java.io.*;
class pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row");
		int row=Integer.parseInt(br.readLine());
		char ch='c';
		for(int i=1; i<=row;i++){
			int no= 1;
			for(int j=1; j<=i; j++){
				if(j%2==0){
					System.out.print(ch++ + " ");
					ch++;
				}
				else{
					System.out.print(no +" ");
				}
				no++;
			}
			System.out.println();
		}
	}
}

