class Ifelsedemo2{
	public static void main(String[] args){
		int age = 27;
		System.out.println("Start code");  // start code
		if(age >=18){                      //condition true
			System.out.println("Eligible for voting");  //eligible for voting
		}
		else{
			System.out.println("Not eligible for voting");
		}
		System.out.println("End code");    // end code
	}
}

