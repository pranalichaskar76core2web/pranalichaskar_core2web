class Switchdemo{
	public static void main(String[] args){
		char data = 'B';
		System.out.println("Before switch");  //before switch
		switch(data){
			case 'A' :
				System.out.println("A"); //false
				break;
			case 'B' :
				System.out.println("B");  //true
				break;
			case 'C' :
				System.out.println("c");  
				break;
			default :
				System.out.println("In default switch");
		}
		System.out.println("After switch");  // after switch
	}
}


