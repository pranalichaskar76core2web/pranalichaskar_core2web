import "dart:io";

void main() {
  print("Enter the Number :");
  int number = int.parse(stdin.readLineSync()!);
  int number1 = number;
  int digit = 0;
  bool flag = false;
  while (number1 > 0) {
    digit = number1 % 10;
    if (digit == 0) flag = true;
    number1 ~/= 10;
  }
  if (flag)
    print("$number is a duck number");
  else
    print("$number is not a duck number");
}

