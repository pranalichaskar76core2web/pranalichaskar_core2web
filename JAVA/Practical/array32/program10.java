import java.io.*;
class array10{
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter rows :");
        int row = Integer.parseInt(br.readLine());

        System.out.println("Enter colums :");
        int col = Integer.parseInt(br.readLine());

        int arr[][] = new int[row][col];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print("Enter Element in row " + (i + 1) + " and column " + (j + 1) + " : ");
                arr[i][j] = Integer.parseInt(br.readLine());
            }
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                if ((i == 0 && (j == 0 || j == col - 1)) || (i == row - 1 && (j == 0 || j == col - 1))) {
                    System.out.print(arr[i][j] + "\t");
                }
            }
        }
    }
}


