import "package:flutter/gestures.dart";
import "package:flutter/material.dart";

void main(){
  runApp(const MyApp());
}

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QuizApp(),
    );
  }
}

class QuizApp extends StatefulWidget{
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class _QuizAppState extends State{
  List<Map> allQuestions=[{

    "question":"Who is the founder of microsoft?",
    "option":["Steve jobs","Bill gates","Lary page","Elon musk"],
    "correctAnswer":1
  },
  {
    "question":"Who is the founder of Google?",
    "option":["Steve jobs","Bill gates","Lary page","Elon musk"],
    "correctAnswer":2
  },
  {
   "question":"Who is the founder of Spacex?",
    "option":["Steve jobs","Bill gates","Lary page","Elon musk"],
    "correctAnswer":3
  },
  {
    "question":"Who is the founder of Meta?",
    "option":["Steve jobs","Bill gates","Lary page","Elon musk"],
    "correctAnswer":0
  },
  {
   "question":"Who is the founder of Apple?",
    "option":["Steve jobs","Bill gates","Lary page","Elon musk"],
    "correctAnswer":1
  },
  ];

int currentQuestionIndex=0;

@override
Widget build(BuildContext context){
  return Scaffold(
    appBar: AppBar(
      title: const Text(
        "Quiz App",
      style:TextStyle(
        fontSize: 50,
        fontWeight: FontWeight.w800,
        color: Colors.orange,
      ) ,
      ),
      centerTitle: true,
      backgroundColor: Colors.blue,
    ),

    body: Column(
      children: [
        const SizedBox(
          height:30,
        ),
        Row(
          children: [
            const SizedBox(
              width: 300,
            ),
            Text(
              "Question: ${currentQuestionIndex+1}/${allQuestions.length}",
              style: const TextStyle(
                fontSize:30,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 50,
        ),
        SizedBox(
          width: 400,
          height: 50,
          child: Text(
            allQuestions[currentQuestionIndex]["question"],
            style: const TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.w600,
              color: Colors.purple,
            ),
          ),
        ),
        const SizedBox(
          height: 40,
        ),
        SizedBox(
          width: 400,
          height: 50,
          child: ElevatedButton(
            onPressed: () {},
            child: Text(
            allQuestions[currentQuestionIndex]["option"][0],
            style: const TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
       ),
        const SizedBox(
          height: 40,
        ),
        SizedBox(
          width: 400,
          height: 60,
          child:ElevatedButton(
            onPressed: () {},
          child: Text(
            allQuestions[currentQuestionIndex]["option"][1],
            style: const TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        ),
       const SizedBox(
          height: 40,
        ),
        SizedBox(
          width: 400,
          height: 60,
          child:ElevatedButton(
            onPressed: () {},
          child: Text(
            allQuestions[currentQuestionIndex]["option"][2],
            style: const TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        ),
                const SizedBox(
          height: 40,
        ),
        SizedBox(
          width: 400,
          height: 60,
          child:ElevatedButton(
            onPressed: () {},
          child: Text(
            allQuestions[currentQuestionIndex]["option"][3],
            style: const TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        ),
      ],
    ),
    floatingActionButton: FloatingActionButton(
      onPressed: () {
        if(currentQuestionIndex < allQuestions.length-1){
          currentQuestionIndex++;
          setState(() {});
        }
      },
      backgroundColor: Colors.blue,
      child: const Icon(
        Icons.forward,
        color:Colors.orange,
      ),
    ),
  );
}
}

