class Switchdemo{
	public static void main(String[] args){
		int num = 5;
		System.out.println("Before Switch");   //Before switch
		switch(num){
			case 1 :
				System.out.println("one"); // false
			case 2 :
				System.out.println("two");  // false
			case 3 :  
				System.out.println("three"); ///false
			case 4 :
				System.out.println("four");  // false
			case 5 :
				System.out.println("five");  // true
		}
		System.out.println("After switch");  // after switch
	}
}

