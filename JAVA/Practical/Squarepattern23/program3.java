import java.io.*;
class Squarepattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row");
		int row = Integer.parseInt(br.readLine());
		char ch='d';
		int num= 5;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				if(j%2==1){
					System.out.print(ch++ + " ");
					ch++;
				}
				else if(j%2==0){
					System.out.print(num++ + " ");
					num++;
				}
				else{
					System.out.print(num + " ");
				}
			}
			System.out.println();
			ch++;
			num++;
		}
	}
}



