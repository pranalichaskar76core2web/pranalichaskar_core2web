class Switchdemo{
	public static void main(String[] args){
		int num = 1;
		System.out.println("Before switch");  // before switch
		switch(num){
			case 1 :
				System.out.println("one"); // true
			case 2 :
				System.out.println("two");  // two
				break;
			case 3 :
				System.out.println("three");
			       break;
			default :	       
				System.out.println("In default state"); 
		}
		System.out.println("After switch");  // after switch
	}
}


