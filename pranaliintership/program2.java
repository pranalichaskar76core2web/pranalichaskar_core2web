import java.util.Scanner;
// Creating a Calculator Class
class Calculator {
     // creating a static method for the addition of two numbers entered by user
     // and taking firstNumber and secondNumber as parameters, you can also take integer data type as input
     static double addTwoNumbers(double firstNumber, double secondNumber) {
         double result = firstNumber + secondNumber;
         return result;
     }
     
     // this method rturns subtraction of the entered number by user
     double subtractTwoNumbers(double firstNumber, double secondNumber){
         if (firstNumber >= secondNumber)
             return firstNumber - secondNumber;
         else {
             double subtract = secondNumber - firstNumber;
             return subtract;
         }
     }
     
     // third method is to return the result of multiplication of the two numbers
     double mulTwoNumbers(double firstNumber, double secondNumber){
        double multiply = firstNumber * secondNumber;
        return  multiply;
     }
     
     // this is the last method which return the division value of that two numbers
     double divTwoNumbers(double firstNumber, double secondNumber){
         if (firstNumber >= secondNumber){
             double div = (firstNumber / secondNumber);
             return div;
         }
         else {
             double div = (secondNumber / firstNumber);
             return div;
         }
     }
     
    // this is our main method where we will take input from the user and return the result 
    //according to the user 
    public static void main(String[] args) {
         
        Scanner scanner = new Scanner(System.in);
         
        System.out.println("Enter First Number: ");
        double firstNumber = scanner.nextDouble();
         
        System.out.println("Enter Second Number: ");
        double secondNumber = scanner.nextDouble();
         
        // making an object of the Calculator class so I can call the methods 
        Calculator calculator = new Calculator();
        // Conditions to choose the operations to be operate on entered numbers by the user 
        System.out.print("Press 1 for Addition, 2 for Subtraction, 3 for Multiplication & 4 for Division: ");
         
        // taking the choice of operation by user
        int press = scanner.nextByte();
        
        // using switch-case function to operate and print the results
        // NOTE: You Can Also Use "If-Else" 
        switch (press) {
            case 1 -> System.out.println(firstNumber + "+" + secondNumber + " = " + calculator.addTwoNumbers(firstNumber, secondNumber));
            case 2 -> System.out.println(firstNumber + "-" + secondNumber + " = " + calculator.subtractTwoNumbers(firstNumber, secondNumber));
            case 3 -> System.out.println(firstNumber + "×" + secondNumber + " = " + calculator.mulTwoNumbers(firstNumber, secondNumber));
            case 4 -> System.out.println(firstNumber + "÷" + secondNumber + " = " + calculator.divTwoNumbers(firstNumber, secondNumber));
        }
     }
}
