import java.io.*;
class Input{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row");
		int row =Integer.parseInt(br.readLine());
		int num=2;
		for(int i=1; i<=4; i++){
			for(int j=1; j<=row-i+1; j++){
				System.out.print(num++ +" ");
				num++;
			}
		System.out.println();
		}
	}
}

