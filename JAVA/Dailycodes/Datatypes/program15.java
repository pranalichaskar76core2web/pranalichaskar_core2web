class C2w_integerdemo
{
	public static void main(String[] args)
	{
		int numInt = 'a';
		float numFloat = numInt;
		System.out.println(numFloat);   // 97.0
		numFloat = 20.005;
		System.out.println(numFloat);  //20.005
	}
}

