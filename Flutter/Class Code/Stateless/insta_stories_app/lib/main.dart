import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title:"Insta",
      home:Homescreen(),
    );
  }
}
class Homescreen extends StatelessWidget{
  const Homescreen({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:const Text(
          "Insta storie",
        style:TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w800,
          color: Colors.black,
        ),
        ),
        centerTitle: true,
        backgroundColor:Colors.blue,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child:  Row(
          children: [
            Container(
              width: 180,
              height: 180,
              decoration: const BoxDecoration(
                color: Colors.amber,
                shape: BoxShape.circle),
              ),
            Container(
              width: 180,
              height: 180,
              decoration: const BoxDecoration(
                color: Colors.blue,
                shape: BoxShape.circle),
              ),
            Container(
              width: 180,
              height: 180,
              decoration: const BoxDecoration(
                color: Colors.pink,
                shape: BoxShape.circle),
              ),
             Container(
              width:180,
              height: 180,
              decoration: const BoxDecoration(
                color: Colors.red,
                shape: BoxShape.circle),
              ),
              Container(
              width: 180,
              height: 180,
              decoration: const BoxDecoration(
                color: Colors.purple,
                shape: BoxShape.circle),
              )
          ],
        ),
      ),
    );
  }
}