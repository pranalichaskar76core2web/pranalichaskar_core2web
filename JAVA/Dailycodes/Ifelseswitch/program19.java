class Switchdemo{
	public static void main(String[] args){
		float num = 1.5;
		System.out.println("Before switch");  // before switch
		switch(num){
			case 1.5:
				System.out.println("1.5");  //true
				break;
			case 2.0 :
				System.out.println("2.0");
				break;
			case 2.5 :
				Sytsem.out.println("2.5");
				break;
			default :
				System.out.println("In default switch");
		}
		System.out.println("After switch");  // after switch
	}
}

