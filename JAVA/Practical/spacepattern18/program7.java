import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the row");
		int row= sc.nextInt();
		for(int i=1; i<=row; i++){
			for(int space=1; space<i; space++){
				System.out.print("\t");
			}
			int num=1;
			for(int j=row; j>=i; j--){
				System.out.print(num++ + "\t");
			}
		System.out.println();
		}
	}
}

