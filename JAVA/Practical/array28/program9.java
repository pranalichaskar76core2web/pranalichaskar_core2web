import java.util.*;
class array9{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.println("Enter Size : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                
                System.out.println("Enter Elements : ");
                for(int i = 0 ; i < arr.length ; i++){
                        arr[i] = sc.nextInt();
                }
		
		int count = 0 ;
		for(int j = 0; j < arr.length ; j++){
	       	int rem = 0;	
               	int rev = 0;
		int temp = arr[j];
			while(temp > 0){
				rem = temp % 10;
				rev = rev * 10 + rem;
				temp /= 10;
			
			}
		
			if( (arr[j] == rev) || (temp > 10)){
				count++;	
			}
		}
		System.out.println("Count of Palindrome Elements :" + count);
	}
}



