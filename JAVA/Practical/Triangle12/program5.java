import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Row");
		int row=Integer.parseInt(br.readLine());
		char ch='D';
		for(int i=1; i<=row;i++){
			for(int j= 1; j<=i; j++){
				System.out.print(ch + " ");
				ch++;
			}
			System.out.println();
		}
	}
}

