import java.util.*;
class Squarepattern{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the row ");
		int row= sc.nextInt();
		for(int i=1; i<=row; i++){
			for (int space=1; space<=row-i; space++){
				System.out.print("   ");
			}
			int num =4;
			for(int j=1; j<=i; j++){
				System.out.print(num+ " ");
				num =num+4;
			}
			System.out.println();
		}
	}
}


