import 'package:flutter/material.dart';

void main() {
  runApp(const AppbarColorApp());
}

class AppbarColorApp extends StatefulWidget {
  const AppbarColorApp({super.key});

  @override
  State<AppbarColorApp>createState()=>_AppbarColorAppState();

  }
  class _AppbarColorAppState extends State<AppbarColorApp>{

  bool colorchange=true;
  @override
  Widget build(BuildContext context) {
  return MaterialApp(
    debugShowCheckedModeBanner:false,
      home: Scaffold(
        appBar: AppBar (
          title: const Text("Appbar color App"),
          centerTitle: true,
          backgroundColor: (colorchange)?Colors.amber:Colors.pink,
        ),
        body: const Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("change Appbar color"),
            ],
          )
        ),
        floatingActionButton: FloatingActionButton(
          onPressed:() {
            if(colorchange){
              colorchange=false;
            }else{
              colorchange=true;
            }
            setState((){});
          },
          child: const Icon (Icons.add),
        ),
      )
    );
  }
}
