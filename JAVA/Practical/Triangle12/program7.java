import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row");
		int row=Integer.parseInt(br.readLine());
		int no= 1;
		char ch='a';
		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
				if(j%2==0){
				System.out.print(ch++ + " ");
				}
				else{
					System.out.print(no + " ");
				}
			}
			System.out.println();
			no++;
		}
	}
}

