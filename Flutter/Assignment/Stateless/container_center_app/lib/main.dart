import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "cantainer",
      home: HomeScreen(),
    );
  }
}
class HomeScreen extends StatelessWidget{
  const HomeScreen({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text ("Container color"),
        centerTitle: true,
        backgroundColor:Colors.blue,
      ),
      body: Center(
      child: Container(
        height: 200,
        width: 200,
        decoration: const BoxDecoration(
          color: Colors.amber,
          shape: BoxShape.rectangle,
        ),
      ),
    )
    );
  }
}
