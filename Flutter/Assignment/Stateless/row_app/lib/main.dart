import "package:flutter/material.dart";
void main(){
  runApp(const MainApp());
}

class MainApp extends StatelessWidget{
    const MainApp({super.key});
    @override
  Widget build(BuildContext context){
    return const MaterialApp(
      title:"Column",
      home: HomeScreen(),
    );
  }
}
class HomeScreen extends StatelessWidget{
  const HomeScreen({super.key});
  @override
Widget build(BuildContext context){
  return Scaffold(
    appBar:AppBar(
      title:const Text ("Column"),
      backgroundColor:Colors.blue,
    ),
    
     body:Row(
          children:[
      Container(
         width:200,
         height:200,
        decoration:const BoxDecoration(
          color:Colors.amber,
          shape:BoxShape.rectangle,
        ),
      ),
          const SizedBox(
          width:20,
          ),
       Container(
         width:200,
         height:200,
        decoration:const BoxDecoration(
          color:Colors.amber,
          shape:BoxShape.rectangle,
        ),
       ),
          ],
    ),
  );
}
 }
