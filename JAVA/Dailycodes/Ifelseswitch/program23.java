class Switchdemo{
	public static void main(String[] args){
		String friends = "Kanha";
		System.out.println("Before switch");
		switch(friends){
			case "Ashish" :
				System.out.println("barclays");
				break;
			case "Kanha" :
				System.out.println("Bmc Software");
				break;
			case "Rahul" :
				System.out.println("Ibm");
				break;
			default :
				System.out.println("In default switch");
		}
		System.out.println("After switch");
	}
}


