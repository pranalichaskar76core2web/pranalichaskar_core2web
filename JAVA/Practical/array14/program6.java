import java.io.*;
class array6{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		for(int i=0; i<arr.length;i++){
			System.out.print("Enter the character of array " +i+ ": ");
			int character=Integer.parseInt(br.readLine());
			arr[i]=character;
		}
		for(int i=0; i<arr.length;i++){
			System.out.print((char)arr[i] +"\t");
		}
	}
}

