import java.io.*;
class Output{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row");
		int row=Integer.parseInt(br.readLine());
		int num=3;
		for(int i=1; i<=3; i++){
			for(int j=1; j<=i; j++){
				System.out.print(num-- + " ");
			}
			System.out.println();
		}
	}
}

