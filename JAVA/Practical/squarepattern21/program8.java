import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the row ");
		int row =Integer.parseInt(br.readLine());
		for(int i=1; i<=row; i++){
			//int num=1;
			char ch='D';
			for(int j=1; j<=row; j++){
				
				if(j%2==0 && i%2==0){
					System.out.print(ch + " ");
					ch--;
				}
				else{
					System.out.print('#' + " ");
				}
				i++;
			}
			System.out.println();
		}
	}
}

