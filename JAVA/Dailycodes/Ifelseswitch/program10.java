class Ifelsedemo{
	public static void main(String[] args){
		int num = 8;
		if(num >0){  //true condition
			System.out.println("Number is positive");  // number is positive
		}
		else if (num<0){ //false condition
			System.out.println("Number is negative");
		}
		else{
			System.out.println("Number is zero");
		}
	}
}

