class Switchdemo{
	public static void main(String[] args){
		int num = 3;
		System.out.println("Before Switch");    //before switch
		switch(num){
			case 1 :
				System.out.println("one");
			case 2 :
				System.out.println("two");
			case 3 :
				System.out.println("Three");  // three
		}
		System.out.println("After switch");  // after switch
	}
}


