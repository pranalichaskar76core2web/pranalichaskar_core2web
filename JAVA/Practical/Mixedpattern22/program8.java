import java.util.*;
class mixedpattern8{
        public static void main(String[] args){
                Scanner sc=new Scanner(System.in);
                System.out.println("Enter the row");
                int row=sc.nextInt();
		int num=4;
                for(int i=1; i<=row; i++){
                        for(int sp=1; sp<=row-i; sp++){
                                System.out.print("\t");
                        }
                        for(int j=1; j<=i; j++){
                                System.out.print(num +"\t");
				num--;
                        }
                        for(int j=2; j<=i; j++){
                                System.out.print(num++ + "\t");
                        }
                System.out.println();
		num++;
                }
        }
}
