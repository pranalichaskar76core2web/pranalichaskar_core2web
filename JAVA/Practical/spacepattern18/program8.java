import java.util.*;
class Demosquare{
	public static void main(String[] args){
		System.out.println("Enter the row");
		Scanner sc= new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1; i<=row; i++){
			int num=i;
			for(int space=1; space<i; space++){
				System.out.print("\t");
			}
			for(int j=row; j>=i; j--){
				System.out.print(num+++ "\t");
			}
		System.out.println();
		}
	}
}

