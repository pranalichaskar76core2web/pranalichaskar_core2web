import java.io.*;
class Number{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number");
		int num=Integer.parseInt(br.readLine());
		int count =0;
		int temp=num;
		while(num>0){
			if(temp%num==0){
				count++;
			}
			num--;
		}
		if(count==2){
			System.out.print(temp + " is a prime number");
		}
		else{
			System.out.print(temp + " is not a prime number");
		}
	}
}


